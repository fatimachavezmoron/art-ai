const Products = [
  { 
    type: 'Sold Out',
    name: 'Jennifer Lawrence',
    image: 'jhhy.jpg',
    rating: '(5.0)',
    description: 'Painting depicting modern urban princess.',
    museum: 'Prado Museum',
    googleMap: 'https://www.google.com/maps/search/Prado+Museum/@40.4141863,-3.6957563,17z/data=!3m1!4b1?entry=ttu'
  },
  {
    type: 'Sold Out',
    name: 'Marylin Monroe',
    image: 'Default_centered_isometric_mural_graffiti_composition_shapes_p_0_c752f681-c179-43e4-a884-c0c8bf0cf345_1.jpg',
    rating: '(4.8)',
    description: 'Portrait, minimalism, bold colors. Centered isometric.',
    museum: 'The Rijksmuseum',
    googleMap: 'https://www.google.com/maps/place/Rijksmuseum/@52.3599976,4.8826439,17z/data=!3m1!4b1!4m6!3m5!1s0x47c609eec1bb16e5:0xd54373ae6a408585!8m2!3d52.3599976!4d4.8852188!16zL20vMDZqc2Y?entry=ttu'
  },
  {
    type: 'Sold Out',
    name: 'The Princess',
    image: 'DreamShaper_v6_Illustration_art_Dongson_drum_patterns_Line_art_012.jpg',
    rating: '(4.3)',
    description: 'Vibrant colors, ultra detailed, acrylic painting.',
    museum: 'The Pompidou Centre Museum',
    googleMap: 'https://www.google.com/maps/place/Centro+Nacional+de+Arte+y+Cultura+Georges+Pompidou/@48.860642,2.3496701,17z/data=!3m1!4b1!4m6!3m5!1s0x47e66e1c09b820a3:0xb7ac6c7e59dc3345!8m2!3d48.860642!4d2.352245!16zL20vMGYzMnA?entry=ttu'
  },
  {
    type: 'Sold Out',
    name: 'Nicole Kidman',
    image: 'she.jpeg',
    rating: '(4.5)',
    description: 'Stylized anime illustration, hyper - detailed illustrations.',
    museum: 'The British Museum',
    googleMap: 'https://www.google.com/maps/place/Museo+Brit%C3%A1nico/@51.5194133,-0.1295315,17z/data=!3m2!4b1!5s0x48761b325be9f49d:0x6898ed8fae9dea8c!4m6!3m5!1s0x48761b323093d307:0x2fb199016d5642a7!8m2!3d51.5194133!4d-0.1269566!16zL20vMDFoYjM?entry=ttu'
  },
  { 
    type: 'Sold Out',
    name: 'Astronaut',
    image: '0001.jpg',
    rating: '(5.0)',
    description: 'Painting depicting modern urban princess.',
    museum: 'Prado Museum',
    googleMap: 'https://www.google.com/maps/search/Prado+Museum/@40.4141863,-3.6957563,17z/data=!3m1!4b1?entry=ttu'
  },
  {
    type: 'Sold Out',
    name: 'Kurt',
    image: '002.jpg',
    rating: '(4.8)',
    description: 'Portrait, minimalism, bold colors. Centered isometric.',
    museum: 'The Rijksmuseum',
    googleMap: 'https://www.google.com/maps/place/Rijksmuseum/@52.3599976,4.8826439,17z/data=!3m1!4b1!4m6!3m5!1s0x47c609eec1bb16e5:0xd54373ae6a408585!8m2!3d52.3599976!4d4.8852188!16zL20vMDZqc2Y?entry=ttu'
  },
  {
    type: 'Sold Out',
    name: 'Penguin',
    image: '0003.jpg',
    rating: '(4.3)',
    description: 'Vibrant colors, ultra detailed, acrylic painting.',
    museum: 'The Pompidou Centre Museum',
    googleMap: 'https://www.google.com/maps/place/Centro+Nacional+de+Arte+y+Cultura+Georges+Pompidou/@48.860642,2.3496701,17z/data=!3m1!4b1!4m6!3m5!1s0x47e66e1c09b820a3:0xb7ac6c7e59dc3345!8m2!3d48.860642!4d2.352245!16zL20vMGYzMnA?entry=ttu'
  },
  {
    type: 'Sold Out',
    name: 'The Koi',
    image: 'DreamShaper_v5_portait_of_a_KOI_FISH_in_the_middle_of_fractals_0.jpg',
    rating: '(4.5)',
    description: 'Stylized anime illustration, hyper - detailed illustrations.',
    museum: 'The British Museum',
    googleMap: 'https://www.google.com/maps/place/Museo+Brit%C3%A1nico/@51.5194133,-0.1295315,17z/data=!3m2!4b1!5s0x48761b325be9f49d:0x6898ed8fae9dea8c!4m6!3m5!1s0x48761b323093d307:0x2fb199016d5642a7!8m2!3d51.5194133!4d-0.1269566!16zL20vMDFoYjM?entry=ttu'
  }
];

module.exports = Products