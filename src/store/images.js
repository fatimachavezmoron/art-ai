export const cubic = [
  {
    id: "0",
    image: "01a1.jpg"
  },
  {
    id: "1",
    image: "01a2.jpg"
  },
  {
    id: "2",
    image: "01a3.jpg"
  },
  {
    id: "3",
    image: "01a4.jpg"
  },
  {
    id: "4",
    image: "01a5.jpg"
  },
  {
    id: "5",
    image: "01a6.jpg"
  },
  {
    id: "6",
    image: "01a7.jpg"
  }

];

export const dina = [
  {
    id: "4",
    image: "01b5.jpg"
  },
  {
    id: "5",
    image: "01b6.jpg"
  },
  {
    id: "0",
    image: "01b1.jpg"
  },
  {
    id: "1",
    image: "01b2.jpg"
  },
  {
    id: "2",
    image: "01b3.jpg"
  },
  {
    id: "3",
    image: "01b4.jpg"
  },
  {
    id: "6",
    image: "01b7.jpg"
  },
  {
    id: "7",
    image: "01b8.jpg"
  }
];

export const alexa = [
  {
    id: "1",
    image: "01c1.jpg"
  },
  {
    id: "2",
    image: "01c2.jpg"
  },
  {
    id: "3",
    image: "01c3.jpg"
  },
  {
    id: "4",
    image: "01c4.jpg"
  },
  {
    id: "5",
    image: "01c5.jpg"
  },
  {
    id: "7",
    image: "01c7.jpg"
  },
  {
    id: "8",
    image: "01c8.jpg"
  },
  {
    id: "9",
    image: "01c9.jpg"
  },
  {
    id: "6",
    image: "01c6.jpg"
  }
];

export const ludovica = [
  {
    id: "1",
    image: "01d1.jpg"
  },
  {
    id: "2",
    image: "01d2.jpg"
  },
  {
    id: "3",
    image: "01d3.jpg"
  },
  {
    id: "4",
    image: "01d4.jpg"
  },
  {
    id: "5",
    image: "01d5.jpg"
  },
  {
    id: "6",
    image: "01d6.jpg"
  },
  {
    id: "7",
    image: "01d7.jpg"
  }
];

export const blue = [
  {
    id: "1",
    image: "01e1.jpg"
  },
  {
    id: "2",
    image: "01e2.jpg"
  },
  {
    id: "3",
    image: "01e3.jpg"
  },
  {
    id: "4",
    image: "01e4.jpg"
  },
  {
    id: "5",
    image: "01e5.jpg"
  },
  {
    id: "6",
    image: "01e6.jpg"
  },
  {
    id: "7",
    image: "01e7.jpg"
  },
  {
    id: "8",
    image: "01e8.jpg"
  },
  {
    id: "9",
    image: "01e9.jpg"
  },
];


export const thomas = [
  {
    id: "1",
    image: "01f1.jpg"
  },
  {
    id: "2",
    image: "01f2.jpg"
  },
  {
    id: "3",
    image: "01f3.jpg"
  },
  {
    id: "4",
    image: "01f4.jpg"
  },
  {
    id: "5",
    image: "01f5.jpg"
  },
  {
    id: "6",
    image: "01f6.jpg"
  },
  {
    id: "7",
    image: "01f7.jpg"
  }
];

// Styles

export const ricePaperTexture = [
  {
    id: "1",
    image: "03a.jpg"
  },
  {
    id: "2",
    image: "03b.jpg"
  },
  {
    id: "3",
    image: "03c.jpg"
  },
  {
    id: "4",
    image: "03d.jpg"
  },
  {
    id: "5",
    image: "03e.jpg"
  }

];

export const watercolor = [
  {
    id: "1",
    image: "kurt0.jpg"
  },
  {
    id: "2",
    image: "kurt1.jpg"
  },
  {
    id: "3",
    image: "03b.jpg"
  },
  {
    id: "4",
    image: "kurt.jpg"
  },
  {
    id: "5",
    image: "kurt3.jpg"
  },
  {
    id: "6",
    image: "kurt2.jpg"
  },
  {
    id: "7",
    image: "01f.jpg"
  },

];

export const vietnamesse = [
  {
    id: "1",
    image: "01a.jpg"
  },
  {
    id: "2",
    image: "01b.jpg"
  },
  {
    id: "3",
    image: "01c.jpg"
  },
  {
    id: "4",
    image: "01d.jpg"
  },
  {
    id: "5",
    image: "01e.jpg"
  },
  {
    id: "6",
    image: "01f.jpg"
  },

];

export const cherry = [
  {
    id: "1",
    image: "02a.jpg"
  },
  {
    id: "2",
    image: "02b.jpg"
  },
  {
    id: "3",
    image: "02c.jpg"
  },
  {
    id: "4",
    image: "02d.jpg"
  },
  {
    id: "5",
    image: "02e.jpg"
  },
  {
    id: "6",
    image: "02f.jpg"
  },
  {
    id: "7",
    image: "02h.jpg"
  }

];

export const cities = [
  {
    id: "1",
    name: 'AMSTERDAM',
    image: "amsterdam.jpg"
  },
  {
    id: "2",
    name:'BARCELONA',
    image: "barcelona.jpg"
  },
  {
    id: "3",
    name: 'BERLIN',
    image: "berlin.jpg"
  },
  {
    id: "4",
    name: 'HAMBURG',
    image: "hamburg.jpg"
  },
  {
    id: "5",
    name: 'LONDON',
    image: "london.jpg"
  },
  {
    id: "6",
    name: 'NEW YORK',
    image: "newYork.jpg"
  },
  {
    id: "7",
    name: 'MOSCOW',
    image: "moscu.jpg"
  },
  {
    id: "8",
    name: 'PARIS',
    image: "paris.jpg"
  },
  {
    id: "9",
    name: 'PRAGA',
    image: "praga.jpg"
  },
  {
    id: "10",
    name: 'TOKYO',
    image: "tokio.jpg"
  },
  {
    id: "11",
    name: 'VENICE',
    image: "venice.jpg"
  }

];

export const discover1 = [
  {
    id: "0",
    image: "002.jpg"
  },
  {
    id: "1",
    image: "001a.jpg"
  },
  {
    id: "2",
    image: "001aa.jpg"
  },
  {
    id: "3",
    image: "01aaa.jpg"
  }

];

export const discover2 = [
  {
    id: "0",
    image: "001b.jpg"
  },
  {
    id: "1",
    image: "001bb.jpg"
  },
  {
    id: "2",
    image: "001bn.jpg"
  },
  {
    id: "3",
    image: "001bq.jpg"
  }

];

export const discover3 = [
  {
    id: "0",
    image: "001.jpg"
  },
  {
    id: "1",
    image: "001c.jpg"
  },
  {
    id: "2",
    image: "001cc.jpg"
  },
  {
    id: "3",
    image: "001ccc.jpg"
  }
];

export const buyPic = [
  {
    id: "1",
    name: 'Paul Atreides',
    size: '70x100',
    price: 25,
    quantity: 1,
    image: "01a.jpg"
  },
  {
    id: "2",
    name: 'Jessica Atreides',
    size: '70x100',
    price: 23,
    quantity: 1,
    image: "01c.jpg"
  },
  {
    id: "3",
    name: 'Irulan Corrino',
    size: '70x100',
    price: 22,
    quantity: 1,
    image: "01f.jpg"
  },
  {
    id: "4",
    name: 'Flying',
    size: '100x140',
    price: 30,
    quantity: 1,
    image: "001w.jpg"
  },
  {
    id: "5",
    name: 'Alice',
    size: '70x100',
    price: 20,
    quantity: 1,
    image: "003w.jpg"
  },
  {
    id: "6",
    name: 'Chiani',
    size: '50x70',
    price: 23,
    quantity: 1,
    image: "01e.jpg"
  },
  {
    id: "7",
    name: 'Duchess',
    size: '100x140',
    price: 32,
    quantity: 1,
    image: "005w.jpg"
  },
  {
    id: "8",
    name: 'The Force',
    size: '70x100',
    price: 25,
    quantity: 1,
    image: "006w.jpg"
  },
  {
    id: "9",
    name: 'Fremen',
    size: '50x70',
    price: 18,
    quantity: 1,
    image: "007w.jpg"
  }
];
