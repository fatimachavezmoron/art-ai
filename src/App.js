import React, {useState} from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar.js';
import Footer from './components/Footer.js';
import Home from './pages/home.js';
import About from './pages/About.jsx';
import Artist from './pages/Artist/Artist.jsx';
import ProductPage from './components/ProductPage.js';
import './assets/styles/layout.css';
import './App.css';
import ScrollToTop from './components/ScrollToTop.js';
import ReactSwitch from 'react-switch';
import { useThemeContext } from './context/ThemeContext';
import ArtistLorenzo from './pages/Artist/ArtistLorenzo.jsx';
import ArtistDina from './pages/Artist/ArtistDina.jsx';
import ArtistAlexa from './pages/Artist/ArtistAlexa.jsx';
import ArtistLudovica from './pages/Artist/ArtistLudovica.jsx';
import ArtistBlueTeam from './pages/Artist/ArtistBlueTeam.jsx';
import ArtistThomas from './pages/Artist/ArtistThomas.jsx';
import ArtStyles from './pages/ArtStyles/ArtStyles.jsx';
import Events from './pages/ArtEvents/Events.jsx';
import Projects from './pages/Projects/Projects.jsx';
import Discover from './pages/Discover/Discover.jsx';
import AppProducts from './pages/BuyProduct/AppProducts';



function App() { 

  const {contextTheme, setContextTheme}= useThemeContext() 
  const [checked, setChecked] = useState(false);
  const handleSwitch = (nextChecked) => {
    setContextTheme( (state) => (state === 'Light' ? 'Dark' : 'Light'))
    setChecked(nextChecked)
  }

  return (  
    <Router>
      <ScrollToTop />
      <div className="App" id={contextTheme}>
        <Navbar />
      <div className='ThemeStyle'>
          <ReactSwitch 
        onChange={handleSwitch}
        checked={checked}
        onColor="#ff4500"
        onHandleColor="#2693e6"
        handleDiameter={30}
        uncheckedIcon={false}
        checkedIcon={false}
        boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
        activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
        height={20}
        width={40}
        className="react-switch"
        id="material-switch"/>
        <h3 className='contextThemeText'>{contextTheme} Mode</h3>
      </div>  
        <Routes>
          <Route path={`${process.env.PUBLIC_URL}/`} element={<Home />} />
          <Route path={`${process.env.PUBLIC_URL}/design/:index`} element={<ProductPage />} />
          <Route path={`${process.env.PUBLIC_URL}/about`} element={<About />} />
          <Route path={`${process.env.PUBLIC_URL}/Artist`} element={<Artist />} />
          <Route path={`${process.env.PUBLIC_URL}/Artist_Lorenzo`} element={<ArtistLorenzo />} />
          <Route path={`${process.env.PUBLIC_URL}/Artist_Dina`} element={<ArtistDina />} />
          <Route path={`${process.env.PUBLIC_URL}/Artist_Alexa`} element={<ArtistAlexa />} />
          <Route path={`${process.env.PUBLIC_URL}/Artist_Ludovica`} element={<ArtistLudovica />} />
          <Route path={`${process.env.PUBLIC_URL}/Artist_Blue_team`} element={<ArtistBlueTeam />} />
          <Route path={`${process.env.PUBLIC_URL}/Artist_Thomas`} element={<ArtistThomas />} />
          <Route path={`${process.env.PUBLIC_URL}/ArtStyles`} element={<ArtStyles />} />
          <Route path={`${process.env.PUBLIC_URL}/Events`} element={<Events />} />
          <Route path={`${process.env.PUBLIC_URL}/Projects`} element={<Projects />} />
          <Route path={`${process.env.PUBLIC_URL}/Discover`} element={<Discover />} />
          <Route path={`${process.env.PUBLIC_URL}/Buy`} element={<AppProducts />} />
        </Routes>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
