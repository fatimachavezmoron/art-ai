import React from 'react';
import Twitter from '../assets/images/twitter.svg'
import youtube from '../assets/images/youtube.svg'
import instagram from '../assets/images/insta.svg'

function Footer() {
  return (
    <div className='footer'>
      <h6>Copyright © 2023 All rights reserved. </h6>
      <a href='https://twitter.com/Fatima79120595' target='_blank' rel='noopener noreferrer'>
      <img src={Twitter} alt='twitter' className='social-contact'/>
      </a>
      <a href='https://www.youtube.com/' target='_blank' rel='noopener noreferrer'>
      <img src={youtube} alt='youtube' className='social-contact'/>
      </a>
      <a href='https://www.instagram.com/fati.chavez.moron/' target='_blank' rel='noopener noreferrer'>
      <img src={instagram} alt='tinstagram' className='social-contact'/>
      </a>
    </div>
  )
}

export default Footer