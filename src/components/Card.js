import React from 'react';
import { Link } from 'react-router-dom';
import Products from '../store/Products';
import Star from '../assets/images/star.svg';
import { motion } from 'framer-motion';
import { useInView } from 'react-intersection-observer';


const Product = () => {
  return (
    <>
      <h1 className='worksExhibition'>Some Works in museum exhibitions</h1>
      <div className='cards'>
        {Products.map((item, index) => (
          <InViewAnimation key={index}>
            <Link to={`/design/${index}`}>
              <motion.img
                src={require(`../assets/images/${item.image}`)}
                alt="Product"
                className="card-img"
                style={{ width: '100%', borderRadius: '10px' }}
                initial={{ opacity: 0, y: 50 }}
                animate={{ opacity: 0.9, y: 0 }}
                transition={{ delay: 0.1, duration: 1 }}
                whileHover={{
                  scale: 1.1,
                  opacity: 1,
                  transition: { duration: 0.2 } 
                }}
                
              />
            </Link>
            <div className="card-body">
              <img src={Star} alt='star' className='card-icon' />
              <span className='five-stars'>{item.rating}</span>
            </div>
            <p className='bold'>{item.name}</p>
            <div className='card-footer'>
              <span className='italic'>Style: Makoto Shinkai.</span>
              <p>{item.description}</p>
              <p>{item.museum}</p>
              <a href={item.googleMap} className='a-map' target='_blank' rel='noopener noreferrer'>View map</a>
            </div>
          </InViewAnimation>
        ))}
      </div>
    </>
  );
};

const InViewAnimation = ({ children }) => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.5,
  });

  return (
    <motion.div
      ref={ref}
      initial={{ opacity: 0, y: 50 }}
      animate={inView ? { opacity: 1, y: 0 } : { opacity: 0, y: 50 }}
      transition={{ delay: 0.1, duration: 1 }}
    >
      {children}
    </motion.div>
  );
};

export default Product;
