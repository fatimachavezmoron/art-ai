import React, {useState} from 'react';
import Image from '../assets/images/she.jpeg';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import close from '../assets/images/close.svg'
import menu from '../assets/images/menu.svg'


function Navbar() {

  const [isOpen, setIsOpen] = useState(false);
  const openMenu = () => {
    setIsOpen(true);
  };

  const closeMenu = () => {
    setIsOpen(false);
  };

  return (
    <>
      <header className='headerNavbar'>
        <img src={menu} alt='menuIcon' onClick={openMenu} className={'open-menu open'}/>
        <motion.div
          initial='hidden'
          animate='visible'
          variants={{
            hidden: { opacity: 0, y: -100 },
            visible: { opacity: 1, y: 0 }
          }}
          transition={{ delay: 0.3, duration: 1.9 }}
          whileInView='visible'
          viewport={{ once: true, threshold: 0.5 }}
        > 
            <nav className={'navBar ' + (isOpen ? 'visible' : '')} id="nav">
              <img className='myImage' src={Image} alt=''/>
              <img src={close} alt='closeIcon' onClick={closeMenu} className="close-menu" id="close"/>
              <ul className='myList_ul'> 
                <li>
                  <Link to={`${process.env.PUBLIC_URL}/`} className='myList' onClick={closeMenu}>
                  Home
                  </Link>
                </li>             
                <li>
                  <Link to={`${process.env.PUBLIC_URL}/Events`} className='myList'  onClick={closeMenu}>
                  Events
                  </Link>  
                </li>               
                <li>
                  <Link to={`${process.env.PUBLIC_URL}/Artist_Lorenzo`} className='myList' onClick={closeMenu}>
                  Artist
                  </Link>  
                </li>                
                <li>
                  <Link to={`${process.env.PUBLIC_URL}/ArtStyles`} className='myList' onClick={closeMenu} >
                  Styles
                  </Link>
                </li>                
                <li>
                  <Link to={`${process.env.PUBLIC_URL}/Projects`} className='myList'  onClick={closeMenu}>
                  Projects
                  </Link>
                </li>             
                <li>
                  <Link to={`${process.env.PUBLIC_URL}/Discover`} className='myList' onClick={closeMenu} >
                  Discover
                  </Link>
                </li>
                <li>
                  <Link to={`${process.env.PUBLIC_URL}/Buy`} className='myList' onClick={closeMenu} > 
                    Buy
                  </Link> 
                </li>
                <li>
                  <Link to={`${process.env.PUBLIC_URL}/about`} className='myList' onClick={closeMenu} > 
                    About
                  </Link> 
                </li>
              </ul>
            </nav> 
          <div className='burguer'>
          </div>
          
        </motion.div>
      </header>  
    </>
  )
}

export default Navbar

