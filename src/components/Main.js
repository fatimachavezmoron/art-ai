import React from 'react';
import Image1 from '../assets/images/01.jpg';
import Image2 from '../assets/images/02.jpeg';
import Image3 from '../assets/images/03.jpg';
import Image4 from '../assets/images/04b.jpg';
import Image5 from '../assets/images/04a.jpg';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';

function Main() {
  return (
    <> 
      <div className='MainContainer'>
        <motion.div 
        initial='hidden'
        animate='visible'
        variants={{
          hidden: { opacity: 0, x: -50 },
          visible: { opacity: 1, x: 0 }
        }}
        transition={{ delay: 0.3, duration: 1.9 }}
        whileInView='visible'
        viewport={{ once: true, threshold: 0.5 }}
        className='images-Cont'>
          <div className='images-Container'>
            <Link to='/Events' className=''>
            <img src={Image1} alt='' className='img-main'/> 
            </Link>
            <div className="descriptionText">
              <p>EVENTS</p>
            </div>
          </div>  
          <div className='images-Container'>
            <Link to='/Artist_Lorenzo' className=''>
            <img src={Image2} alt='' className='img-main'/> 
            </Link>
            <div className="descriptionText">
              <p>ARTIST</p>
            </div>
          </div>  
          <div className='images-Container'>
            <Link to='/ArtStyles' className=''>
            <img src={Image3} alt='' className='img-main'/> 
            </Link>
            <div className="descriptionText">
              <p>STYLES</p>
            </div>
          </div>  
          <div className='images-Container'>
            <Link to='/Projects' className=''>
            <img src={Image4} alt='' className='img-main'/> 
            </Link>
            <div className="descriptionText">
              <p>PROJECTS</p>
            </div>
          </div>  
          <div className='images-Container'>
            <Link to='/Discover' className=''>
            <img src={Image5} alt='' className='img-main'/> 
            </Link>
            <div className="descriptionText">
              <p>DISCOVER</p>
            </div>
          </div>  
        </motion.div>
        <motion.div 
          initial='hidden'
          animate='visible'
          variants={{
            hidden: { opacity: 0, x: -100 },
            visible: { opacity: 1, x: 0 }
          }}
          transition={{ delay: 0.3, duration: 1.9 }}
          whileInView='visible'
          viewport={{ once: true, threshold: 0.5 }}
        className='textMainCont'>
          <h2 className='title-main2'>"Discovering new artistic dimensions <br/>
          with the innovation brought by artificial intelligence."</h2>
          <h1 className='title-main'>Online experiences</h1>
          <p className='par-main'>The rise of digital technology has given an entirely new meaning 
          to what art is, and how it can be experienced. New artwork has emerged designed 
          by Artificial Intelligence using Machine learning. With it the dilemma has been born 
          – is Artificial intelligence-designed artwork real creative art?
          <br/>
          Art and AI technology combine forces to create works of art that interact 
          with people.  Principles of design are used in AI art projects that 
          allow the creator and the viewer to explore their creativity through play.</p>

          <h1 className='title-main'>Art always looked for creative innovation</h1>
          <p className='par-main'>New schools and tendencies came to life over the decades, 
          and so AI comes into play with full rights. <br/>
          Each new approach has the key goal to 
          challenge the way people think about the world, by using art as a medium for change. 
          With AI technology maturing it enters all fields of life at an exponential rate. 
          It will have a significant impact on changing our everyday lives and so the art human 
          beings create. 
          AI questions the future of creativity. <br/>
          Art and AI technology combine forces to create works of art that interact 
          with people.  Principles of design are used in AI art projects that 
          allow the creator and the viewer to explore their creativity through play.</p>

          <h1 className='title-main'>When artificial intelligence creates artworks, 
          can they be considered valuable?</h1>
          <p className='par-main'>Artificial Intelligence Art can be created autonomously by 
          AI systems and in collaboration between a human and an AI system. The creation process is the most
          important area where artificial intelligence can make a difference. Using deep machine learning 
          technology, it can create visual stories that have never been seen before. 
          So, no one has not to worry, that someone before already has created something similar.</p>

        </motion.div>
      </div>  
    </>
  )
}

export default Main