import React from 'react';
import { useParams } from 'react-router-dom';
import Products from '../store/Products';
import { motion } from 'framer-motion';


const ProductPage = () => {
  const { index } = useParams();
  const product = Products[index];

  return (
    <div className='Product-container'>
      <span className='circleStyle0'></span>
      <span className='circleStyle1'></span>
      <motion.img
        initial='hidden'
        animate='visible'
        variants={{
          hidden: { opacity: 0, y: 50 },
          visible: { opacity: 1, y: 0 }
        }}
        transition={{ delay: 0.2, duration: 1.9 }}
        whileInView='visible'
        viewport={{ once: true, threshold: 0.5 }}
        src={require(`../assets/images/${product.image}`)}
        alt="Product"
        className='Product-img'
      />
      <motion.h2
        initial='hidden'
        animate='visible'
        variants={{
          hidden: { opacity: 0, y: 50 },
          visible: { opacity: 1, y: 0 }
        }}
        transition={{ delay: 0.3, duration: 1.9 }}
        whileInView='visible'
        viewport={{ once: true, threshold: 0.5 }}
      >{product.name}</motion.h2>
      <motion.h1
        initial='hidden'
        animate='visible'
        variants={{
          hidden: { opacity: 0, y: 50 },
          visible: { opacity: 1, y: 0 }
        }}
        transition={{ delay: 0.3, duration: 1.9 }}
        whileInView='visible'
        viewport={{ once: true, threshold: 0.5 }}
      >{product.museum}</motion.h1>
      <motion.p
      initial='hidden'
      animate='visible'
      variants={{
        hidden: { opacity: 0, y: 50 },
        visible: { opacity: 1, y: 0 }
      }}
      transition={{ delay: 0.3, duration: 1.9 }}
      whileInView='visible'
      viewport={{ once: true, threshold: 0.5 }}
      >{product.description}</motion.p>
    
    </div>
  );
};

export default ProductPage;


