import React from 'react';
import { cities } from '../../store/images';


const Events = () => {

  return (
    <>
      <div className='allContainer'>
        <div 
           className='EventCont'>
          <span className='circleStyle10'></span>
          <h1 className='artistName'>EVENTS</h1>
          <div className='citiesContainer'>
            {cities.map((item) => (
              <div key={item.id} className='cityItem'>
                <span className='circleStyle10'></span>
                <img
                  src={require(`../../assets/images/cities/${item.image}`)}
                  alt='city_image'
                  className='cityImage'
                />
                <p className='cityName'>{item.name}</p>
              </div>
            ))}
          </div>
        </div>
        <div className='infoContainer'>
          <div className='itemInfoCont'>
              <h2>FIART</h2>
              <h3>NOVEMBER 24</h3>
              <hr />
              <h4>Amsterdam International Art Fair</h4>
              <hr />
              <p>On November 24 and 25, 2023, 
                FIABCN returns to the Museu Maritim of 
                Amsterdam for its 8th edition.</p>
          </div>
          <div className='itemInfoCont'>
              <h2>MACBA</h2>
              <h3>FEBRUARY 20</h3>
              <hr />
              <h4>Barcelona Museum of Contemporary Art</h4>
              <hr />
              <p>Is located in the center 
                of the Raval neighborhood, where you can enjoy contemporary 
                art that the museum offers as well as an extensive program 
                related to visual arts.</p>
          </div>
          <div className='itemInfoCont'>
              <h2>BMUSEUM</h2>
              <h3>30 MARCH</h3>
              <hr />
              <h4>Berlin International Art Fair</h4>
              <hr />
              <p>On November 24 and 25, 2023, 
                FIABCN returns to the Museu Maritim of 
                Germany for its 8th edition.</p>
          </div>
          <div className='itemInfoCont'>
              <h2>MNHA</h2>
              <h3>APRIL 15</h3>
              <hr />
              <h4>Hamburg International Art Fair</h4>
              <hr />
              <p>On November 24 and 25, 2023, 
                FIABCN returns to the Museu Maritim of 
                Germany for its 8th edition.</p>
          </div>
          <div className='itemInfoCont'>
              <h2>L-GRAPH</h2>
              <h3>MAY 12</h3>
              <hr />
              <h4>London International Art Fair</h4>
              <hr />
              <p>On November 24 and 25, 2023, 
                FIABCN returns to the Museu Maritim of 
                England for its 8th edition.</p>
          </div>
          <div className='itemInfoCont'>
              <h2>N-MUSEUM</h2>
              <h3>JUNE 20</h3>
              <hr />
              <h4>New York International Art Fair</h4>
              <hr />
              <p>On November 24 and 25, 2023, 
                FIABCN returns to the Museu Maritim of 
                America for its 8th edition.</p>
          </div>
          <div className='itemInfoCont'>
              <h2>MOSM</h2>
              <h3>03 AUGUST</h3>
              <hr />
              <h4>Moscow International Art Fair</h4>
              <hr />
              <p>On November 24 and 25, 2023, 
                FIABCN returns to the Museu Maritim of 
                Russia for its 8th edition.</p>
          </div>
          <div className='itemInfoCont'>
              <h2>ARTFR</h2>
              <h3>02 SEPTEMBER</h3>
              <hr />
              <h4>Paris International Art Fair</h4>
              <hr />
              <p>On November 24 and 25, 2023, 
                FIABCN returns to the Museu Maritim of 
                France Museum for its 8th edition.</p>
          </div>
          <div className='itemInfoCont'>
              <h2>PRART</h2>
              <h3>SEPTEMBER 21</h3>
              <hr />
              <h4>Praga International Art Fair</h4>
              <hr />
              <p>On November 24 and 25, 2023, 
                FIABCN returns to the Museu Maritim of 
                Praga for its 8th edition.</p>
          </div>
          <div className='itemInfoCont2'>
              <h2>JP-ART</h2>
              <h3>03 OCTOBER</h3>
              <hr />
              <h4>Tokyo International Art Fair</h4>
              <hr />
              <p>On November 24 and 25, 2023, 
                FIABCN returns to the Museu Maritim of 
                Japan for its 8th edition.</p>
          </div>
          <div className='itemInfoCont2'>
              <h2>ITART</h2>
              <h3>NOVEMBER 11</h3>
              <hr />
              <h4>Venice International Art Fair</h4>
              <hr />
              <p>On November 24 and 25, 2023, 
                FIABCN returns to the Museu Maritim of 
                Italy for its 8th edition.</p>
          </div>
        </div>
      </div>  
    </>
  );
};

export default Events;
