import React,{useState} from 'react'
import {ricePaperTexture} from '../../store/images';
import {watercolor} from '../../store/images';
import {vietnamesse} from '../../store/images';
import {cherry} from '../../store/images';
import { motion } from "framer-motion"


const ArtStyles = () => {
  const [index, setIndex] = useState(0);
  const [index1, setIndex1] = useState(0);
  const [index2, setIndex2] = useState(0);
  const [index3, setIndex3] = useState(0);

  function nextStep(setIndexFunc, length) {
    setIndexFunc((prevIndex) => (prevIndex + 1) % length);
  }

  function prevStep(setIndexFunc, length) {
    setIndexFunc((prevIndex) => (prevIndex - 1 + length) % length);
  }

  const currentImage = ricePaperTexture[index];
  const currentImage1 = watercolor[index1];
  const currentImage2 = vietnamesse[index2];
  const currentImage3 = cherry[index3];

  return (
    <>
      <motion.div 
        initial='hidden'
        animate='visible'
        variants={{
          hidden: { opacity: 0, x: -50 },
          visible: { opacity: 1, x: 0 }
        }}
        transition={{ delay: 0.1, duration: 1.9 }}
        whileInView='visible'
        viewport={{ once: true, threshold: 0.5 }}
        className='EachStylCont'>
          <h1 className='artistName'>
          RICE PAPER TEXTURE
          </h1>
        <div className='stylesContainer' >
          <div className="itemStyleCont">
            <img 
              src={require(`../../assets/images/japan/${currentImage.image}`)} 
              alt="slides" 
              className="slides" />
            <button className='prevButton' onClick={() => prevStep(setIndex, ricePaperTexture.length)}>◀</button>
            <button className='nextButton' onClick={() => nextStep(setIndex, ricePaperTexture.length)}>▶</button>
          </div>
        </div>
          <div className='contTextStyle'>
            <div className='textStyle3'></div>
                <p className='eventP'><span className='underline'>Prompt</span>: muted chinese ink painting, muted colors, rice paper texture, 
                splash paint, Ninh Binh town, boat, red sun,  Old cottage. Lakeside. 
                Morning light. Clouds wet to wet techniques. </p>
          </div>
      </motion.div>    
      <motion.div 
        initial='hidden'
        animate='visible'
        variants={{
          hidden: { opacity: 0, x: -50 },
          visible: { opacity: 1, x: 0 }
        }}
        transition={{ delay: 0.1, duration: 1.9 }}
        whileInView='visible'
        viewport={{ once: true, threshold: 0.5 }}
        className='EachStylCont'>

          <h1 className='artistName'>
            BLUE INDIGO AND ORANGE PEN
          </h1>
        <div className='stylesContainer' >
          <div key={currentImage1.id} className="itemStyleCont">
            <img 
              src={require(`../../assets/images/watercolor_style/${currentImage1.image}`)} 
              alt="slides" 
              className="slidesWater" />
            <button className='prevButton' onClick={() => prevStep(setIndex1, watercolor.length)}>◀</button>
            <button className='nextButton' onClick={() => nextStep(setIndex1, watercolor.length)}>▶</button>
          </div>   
        </div>
        <div className='contTextStyle'>
            <div className='textStyle2'></div>
                <p className='eventP'><span className='underline'>Prompt</span>: Ball point pen Ink drawing of beautiful kurt cobain, 
              orange, blue indigo and orange pen, captured in dynamic, full body, 
              Peter Draws, digital illustration, comic style, Dong Son drum patterns background, 
              black and white contrast.perfect anatomy, centered, dynamic, highly detailed, 
              watercolor painting, artstation, concept art, smooth, sharp focus, illustration, 
              art by Carne Griffiths and Wadim Kashin , </p>
            </div>
        </motion.div>
      <div className='EachStylCont'>
        <h1 className='artistName'>LINE ART AND FLAT COLORS</h1>
        <div className='stylesContainer' >
          <div key={currentImage2.id} className="itemStyleCont">
            <img 
              src={require(`../../assets/images/viet_style/${currentImage2.image}`)} 
              alt="slides" 
              className="slidesWater" />
            <button className='prevButton' onClick={() => prevStep(setIndex2, vietnamesse.length)}>◀</button>
            <button className='nextButton' onClick={() => nextStep(setIndex2, vietnamesse.length)}>▶</button>
          </div>              
        </div>
        <div className='contTextStyle'>
            <div className='textStyle1'></div>
            <p className='eventP'><span className='underline'>Prompt</span>: Illustration art, Dongson drum patterns, Line art and flat colors, 
              An expressionistic oil painting european Woman in aodai in the style of Jack Hughes, 
              a masterpiece, highly detailed, digital painting, best quality, high-res, detailed work, 
              post-processing, perfect result </p>
            </div>
        </div>

      <div className='EachStylCont'>
        <h1 className='artistName'>CHERRY BLOSSOM EYES</h1>
        <div className='stylesContainer' >
          <div key={currentImage3.id} className="itemStyleCont">
            <img 
              src={require(`../../assets/images/cherry_style/${currentImage3.image}`)} 
              alt="slides" 
              className="slides" />
            <button className='prevButton' onClick={() => prevStep(setIndex3, cherry.length)}>◀</button>
            <button className='nextButton' onClick={() => nextStep(setIndex3, cherry.length)}>▶</button>
          </div>              
        </div>
          <div className='contTextStyle'>
             <div className='textStyle'></div>
            <p className='eventP'><span className='underline'>Prompt</span>: A detailed illustration "gandalf", 
              facing forward, cherry blossom eyes, no body, no torso, t-shirt design, 
              cherry blossom splash in blue and orange color, t-shirt design, in the style 
              of Studio Ghibli, pastel tetradic colors, 3D vector art, cute and quirky, fantasy art, 
              watercolor effect, bokeh, Adobe Illustrator, hand-drawn, digital painting, low-poly, 
              soft lighting, bird's-eye view, isometric style, retro aesthetic, focused on the character, 
              4K resolution, photorealistic rendering, using Cinema 4D</p>
          </div>
      </div>
    </>
  )
}

export default ArtStyles


