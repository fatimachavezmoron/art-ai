import React from 'react'
// import { motion } from 'framer-motion';
import { discover1 } from '../../store/images'
import { discover2 } from '../../store/images'
import { discover3 } from '../../store/images'
import image1 from '../../assets/images/discover/001/001.jpg';
import image2 from '../../assets/images/discover/002/001bs.jpg';
import image3 from '../../assets/images/discover/003/002.jpg';



const Discover = () => {
  return (
    <>
      {/* <motion.div 
        className='slide-in'
        initial={{scaleY: 0}}
        animate={{scaleY: 0}}
        exit={{scaleX: 1}}
        transition={{ duration: 1, ease: [0.22, 1, 0.36, 1]}}
      />
      <motion.div 
        className='slide-out'
        initial={{scaleY: 1}}
        animate={{scaleY: 0}}
        exit={{scaleX: 0}}
        transition={{ duration: 1, ease: [0.22, 1, 0.36, 1]}}
      /> */}
      <div className='allContainer'>
        <div className='discoverCont'>
          <div className='blockImg'>
            <img src={image1} alt='image1' className='image1'/>
            <div className='smallPics'>
              {discover1.map((item, index) => (
                <img 
                src={require(`../../assets/images/discover/001/${item.image}`)}
                alt='imgDiscover'
                className='smallPicsItem'
                />
              ))}
            </div>
          </div>
          <div className='blockImg'>
            <img src={image2} alt='image1' className='image1'/>
            <div className='smallPics'>
              {discover2.map((item, index) => (
                <img 
                src={require(`../../assets/images/discover/002/${item.image}`)}
                alt='imgDiscover'
                className='smallPicsItem'
                />
              ))}
            </div>
          </div>
          <div className='blockImg'>
            <img src={image3} alt='image1' className='image1'/>
            <div className='smallPics'>
              {discover3.map((item, index) => (
                <img 
                src={require(`../../assets/images/discover/003/${item.image}`)}
                alt='imgDiscover'
                className='smallPicsItem'
                />
              ))}
            </div>
          </div>
        </div>

          <div className='discoverTextCont'>
            <div className='textAlign'>
              <h1>Alessandro Paglia</h1>
              <h4>November 2023</h4>
              <p>
              We stand before you today to introduce an extraordinary 
              artist whose work embodies the fusion of creativity and cutting-edge technology.
               It is our great pleasure to present an artist who has mastered the art of 
               artificial intelligence and brought forth a new realm of possibilities in 
               the world of art.
              In this era of rapidly advancing technology, we are witnessing the rise 
              of artificial intelligence as a transformative force across various industries. 
              Art is no exception to this revolution, and our featured artist stands at the 
              forefront of this exciting movement, harnessing the power of artificial 
              intelligence to create awe-inspiring masterpieces.
              </p>
            </div>
            <div className='textAlign'>
              <h1>Vincenzo Cassano</h1>
              <h4>Dicember 2023</h4>
              <p>
              Through meticulous programming, experimentation, 
              and a profound understanding of both art and technology, 
              we have embarked on a remarkable journey, pushing the boundaries 
              of traditional artistic expression. By combining the inherent 
              creativity of human imagination with the infinite potential of 
              artificial intelligence algorithms, we have unleashed a new wave 
              of artistic exploration.
              Using sophisticated machine learning techniques, we have 
              developed algorithms that can generate original artwork, 
              imbued with a unique style that is both captivating and thought-provoking. 
              These algorithms learn from vast datasets, absorbing the nuances of art history, 
              cultural references, and our own personal style, culminating in the production 
              of astonishing pieces that challenge our perception of creativity.
              </p>
            </div>
            <div className='textAlign'>
              <h1>Mary Cassatt</h1>
              <h4>January 2024</h4>
              <p>
              In a world where originality is highly valued, 
              one might question the authenticity of AI-generated art. 
              However, we see our work as a collaboration between human 
              creativity and artificial intelligence, where our intent and 
              vision are translated into digital brushstrokes by the machine. 
              The resulting creations bear our unmistakable imprint, while 
              simultaneously embracing the innovative potential of AI.
              The impact of this artistic exploration extends far beyond 
              the art world. It prompts us to question our notions of authorship, 
              creativity, and the relationship between humans and machines. 
              It challenges us to ponder the role of technology in shaping the 
              future of art and how we engage with it as viewers and appreciators.
              </p>
            </div>
          </div>     
      </div>    
    </>
  )
}

export default Discover