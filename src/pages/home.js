import React from 'react';
import Product from '../components/Card.js';
import Main from '../components/Main.js';

function Home() {
  return (
    <>
      <Main />
      <Product />
    </>
  );
}

export default Home;
