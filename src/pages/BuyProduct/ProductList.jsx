import React from 'react'
import { buyPic } from '../../store/images'

const ProductList = ({
  allProducts, 
  setAllProducts, 
  countProducts, 
  setCountProducts,
  total,
  setTotal,
}) => {

  const onAddProduct = product => {
if(allProducts.find(item => item.id === product.id)) {
  const products = allProducts.map(item => 
    item.id === product.id 
    ? {...item, quantity: item.quantity + 1} 
    : item 
    );
    setTotal(total + product.price * product.quantity);    
    setCountProducts(countProducts + product.quantity); 
    return setAllProducts([...products])

}
    setTotal(total + product.price * product.quantity);    
    setCountProducts(countProducts + product.quantity); 
    setAllProducts([...allProducts, product]);
    
   };
   console.log(allProducts)

  return (
    <div className='contBuyImg'>
      {buyPic.map((item, index) => (
        <div key={item.id} >
        <img src={require(`../../assets/images/buy/${item.image}`)}   
        alt='buyImg'
        className='buyImg'
        />
          <div className='InfobuyCont' key={item.id}>
          <p>Name: {item.name}</p>  
          <p>Size: {item.size}</p>
          <p>Price: €  {item.price}</p>
          <button onClick = {() => onAddProduct(item)}>Add to Cart</button>
          </div>
        </div>
      ))} 
    </div>
  )
}

export default ProductList