import React, {useState} from 'react'
import BuyProducts from './BuyProducts'
import ProductList from './ProductList'

const AppProducts = () => {
const [allProducts, setAllProducts] = useState([])
const [total, setTotal] = useState(0)
const [countProducts, setCountProducts] = useState(0)

  return (
    <>
      <BuyProducts 
      allProducts = {allProducts}
      setAllProducts = {setAllProducts}
      total= {total}
      setTotal = {setTotal}
      countProducts = {countProducts}
      setCountProducts = {setCountProducts}
      />
      <ProductList       
      allProducts = {allProducts}
      setAllProducts = {setAllProducts}
      total= {total}
      setTotal = {setTotal}
      countProducts = {countProducts}
      setCountProducts = {setCountProducts}/>
    </>
  )
}

export default AppProducts