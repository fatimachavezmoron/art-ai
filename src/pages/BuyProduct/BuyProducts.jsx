import React, { useEffect, useState } from 'react';
import cart from '../../assets/images/buy/cart.svg';
import close from '../../assets/images/close.svg';


const BuyProducts = ({
  allProducts, 
  setAllProducts, 
  total, 
  setTotal,
  countProducts,
  setCountProducts,
}) => {

  const [active, setActive] = useState(false);
  const onDeleteProduct = product => {
    const results = allProducts.filter(item => item.id !== product.id);

    setTotal(total - product.price * product.quantity);    
    setCountProducts(countProducts - product.quantity); 
    setAllProducts(results)
  };

  const onClearCart = () => { 
    setAllProducts([]);
    setTotal(0);
    setCountProducts(0);
  }

  useEffect (() => {
    const btnCart = document.querySelector('.cartIcon')
    const ContainerCartProducts = document.querySelector('.container-cart-products')

    const handleClick = () => { 
      ContainerCartProducts.classList.toggle('hidden-cart');
    };
    if (btnCart) {
      btnCart.addEventListener('click', handleClick);
    }
    return () => {
      if (btnCart) {
        btnCart.removeEventListener('click', handleClick);
      }
    };
  }, []);

  return (
    <>
    <div className='iconCont'>
      <div
      className='container-cart-icon'
      >
        <img src={cart} alt='cartIcon' className='cartIcon' 
        onClick={() => setActive(!active)}/>
        <div className='count-products'>
          <span id='contador-productos'>{countProducts}
          </span> 
        </div>
      </div>

      <div className={`container-cart-products ${active ? '' : 'hidden-cart'}` }>
            <img src={close} 
              alt='closeIcon' 
              className='closeIcon2' 
              onClick={() => setActive(!active)}       
            />
        {
          allProducts.length ? (
            <>
            <div className='row-product'>
              {allProducts.map(product => (
                 <div key={product.id} className='cart-product'>
                 <div className='info-cart-products'>
                   <span className='cantidad-producto-carrito'>
                    {product.quantity}
                   </span>
                   <span className='title-product-cart'>
                   {product.name}
                   </span>
                   <span className='price-product-cart'> &euro;&nbsp;{product.price}
                   </span>
                 </div>
                 <div>
                   <img src={close} 
                   alt='closeIcon' 
                   className='closeIcon'
                   onClick={() => onDeleteProduct(product)}
                   />
                 </div>
               </div>
              ))}
            </div>

            <div className='cart-total'>
              <h3>Total</h3>
              <span className='total-pay'>&euro;&nbsp;{total}</span>
            </div>
            </>
          ) : (
            <p className='cart-empty'>Cart Empty</p>
          )
        }
       <button className='btn-clear-all' onClick={onClearCart}>Empty Cart</button>
      </div>
    </div>
    </>
  )
}

export default BuyProducts