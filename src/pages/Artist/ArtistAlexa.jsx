import React from 'react'
import Alexa from '../../assets/images/artist/01c.jpg'
import {alexa} from '../../store/images';
import { motion } from 'framer-motion';
import Artist from './Artist';

const ArtistAlexa = () => {

    return (
      <>
        <div>
          <Artist />
          <div className='worksContainer'>
            <div className='artistCont'>
              <h1 className='artistName'>ALEXA GARCIA</h1>
            </div>
            <motion.div className='gallery'>
              <motion.div className='slider' drag='x' 
               dragConstraints={{right: 0, left:-3000}} >
                {alexa.map((item, index) => (
                  <motion.div key={index} className="item">
                    <img 
                    src={require(`../../assets/images/artist/01c/${item.image}`)} 
                    alt="Product" 
                    className="ai-img" />
                  </motion.div>
                ))}
              </motion.div>
            </motion.div>
          </div>
          <div className='profileCont'>
              <div>
                <img src={Alexa} alt='alexa_picture' className='imgArtist'/>
              </div>
              <div>
                <p className='profileText'>Alexa Garcia es un Artista visual apasionado, 
                  con un historial destacado en diferentes medios artísticos. 
                  Gran experiencia en ilustración y animación. Acostumbrado a 
                  trabajar de forma independiente y en colaboración en proyectos diversos, 
                  siempre buscando la innovación y la belleza visual. </p>
                  <p>
                    Contact: <span className='email'>alexGarcia_ia_art@works.com</span>
                  </p>
              </div>
          </div>    
        </div>
      </>
    )
}

export default ArtistAlexa