import React from 'react'
import Thomas from '../../assets/images/artist/01f.jpg'
import {thomas} from '../../store/images';
import { motion } from 'framer-motion';
import Artist from './Artist';

const ArtistThomas = () => {
  return (
    <>
      <div>
        <Artist />
        <div className='worksContainer'>
          <div className='artistCont'>
            <h1 className='artistName'>THOMAS WALSH</h1>
          </div>
          <motion.div className='gallery'>
            <motion.div className='slider' drag='x' 
             dragConstraints={{right: 0, left:-2300}} >
              {thomas.map((item, index) => (
                <motion.div key={index} className="item">
                  <img 
                  src={require(`../../assets/images/artist/01f/${item.image}`)} 
                  alt="Product" 
                  className="ai-img" />
                </motion.div>
              ))}
            </motion.div>
          </motion.div>
        </div>
        <div className='profileCont'>
            <div>
              <img src={Thomas} alt='alexa_picture' className='imgArtist'/>
            </div>
            <div>
              <p className='profileText'>Thomas Walsh es un Artista visual apasionado, 
                con un historial destacado en diferentes medios artísticos. 
                Gran experiencia en ilustración y animación. Acostumbrado a 
                trabajar de forma independiente y en colaboración en proyectos diversos, 
                siempre buscando la innovación y la belleza visual. </p>
                <p>
                  Contact: <span className='email'>walshThomas_ia_art@works.com</span>
                </p>
            </div>
        </div>    
      </div>
    </>
  )
}

export default ArtistThomas