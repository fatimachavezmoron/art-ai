import Dina from '../../assets/images/artist/01b.jpg'
import {dina} from '../../store/images';
import { motion } from 'framer-motion';
import Artist from './Artist';

const ArtistDina = () => {
  return (
    <>
      <div>
        <Artist />
        <div className='worksContainer'>
          <div className='artistCont'>
            <h1 className='artistName'>DINA KUZNETSOV</h1>
          </div>
          <motion.div className='gallery'>
            <motion.div className='slider' drag='x' 
             dragConstraints={{right: 0, left:-2800}} >
              {dina.map((item, index) => (
                <motion.div key={index} className="item">
                  <img 
                  src={require(`../../assets/images/artist/01b/${item.image}`)} 
                  alt="Product" 
                  className="ai-img" />
                </motion.div>
              ))}
            </motion.div>
          </motion.div>
        </div>
        <div className='profileCont'>
            <div>
              <img src={Dina} alt='dina_picture' className='imgArtist'/>
            </div>
            <div>
              <p className='profileText'>Dina Kuznetsov es un Artista visual apasionado, 
                con un historial destacado en diferentes medios artísticos. 
                Gran experiencia en ilustración y animación. Acostumbrado a 
                trabajar de forma independiente y en colaboración en proyectos diversos, 
                siempre buscando la innovación y la belleza visual. </p>
                <p>
                  Contact: <span className='email'>dinaKuznetsov_ia_art@works.com</span>
                </p>
            </div>
        </div>    
      </div>
    </>
  )
}

export default ArtistDina