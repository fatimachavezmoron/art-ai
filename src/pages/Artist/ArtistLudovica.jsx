import React from 'react'
import Ludovica from '../../assets/images/artist/01d.jpg'
import {ludovica} from '../../store/images';
import { motion } from 'framer-motion';
import Artist from './Artist';

const ArtistLudovica = () => {
  return (
    <>
      <div>
        <Artist />
        <div className='worksContainer'>
          <div className='artistCont'>
            <h1 className='artistName'>LUDOVICA KHAN</h1>
          </div>
          <motion.div className='gallery'>
            <motion.div className='slider' drag='x' 
             dragConstraints={{right: 0, left:-2300}} >
              {ludovica.map((item, index) => (
                <motion.div key={index} className="item">
                  <img 
                  src={require(`../../assets/images/artist/01d/${item.image}`)} 
                  alt="Product" 
                  className="ai-img" />
                </motion.div>
              ))}
            </motion.div>
          </motion.div>
        </div>
        <div className='profileCont'>
            <div>
              <img src={Ludovica} alt='dina_picture' className='imgArtist'/>
            </div>
            <div>
              <p className='profileText'>Ludovica Khan es un Artista visual apasionado, 
                con un historial destacado en diferentes medios artísticos. 
                Gran experiencia en ilustración y animación. Acostumbrado a 
                trabajar de forma independiente y en colaboración en proyectos diversos, 
                siempre buscando la innovación y la belleza visual. </p>
                <p>
                  Contact: <span className='email'>ludovica_ia_art@works.com</span>
                </p>
            </div>
        </div>    
      </div>
    </>
  )
}

export default ArtistLudovica