import React from 'react'
import { Link } from 'react-router-dom'

const Artist = () => {
  return (
    <div>
       <nav className='navBar2'>
        <ul className='myListUl'>
          <Link to={`${process.env.PUBLIC_URL}/Artist_Lorenzo`} className='myList2'>
          <li><span className='separator'>|</span>Lorenzo</li>
          </Link>
          <Link to={`${process.env.PUBLIC_URL}/Artist_Dina`} className='myList2'>
          <li><span className='separator'>|</span>Dina</li>
          </Link>
          <Link to={`${process.env.PUBLIC_URL}/Artist_Alexa`} className='myList2'>
          <li><span className='separator'>|</span>Alexa</li>
          </Link>
          <Link to={`${process.env.PUBLIC_URL}/Artist_Ludovica`} className='myList2'>
          <li><span className='separator'>|</span>Ludovica</li>
          </Link>
          <Link to={`${process.env.PUBLIC_URL}/Artist_Blue_team`} className='myList2'>
          <li><span className='separator'>|</span>Blue Team</li>
          </Link>
          <Link to={`${process.env.PUBLIC_URL}/Artist_Thomas`} className='myList2'>
          <li><span className='separator'>|</span>Thomas</li>
          </Link>
        </ul>
      </nav>
      <div>

      </div>
    </div>
  )
}

export default Artist