import React from 'react'
import Blue from '../../assets/images/artist/01e.jpg'
import {blue} from '../../store/images';
import { motion } from 'framer-motion';
import Artist from './Artist';

const ArtistBlueTeam = () => {
  return (
    <>
      <div>
      <Artist />
        <div className='worksContainer'>
          <div className='artistCont'>
            <h1 className='artistName'>BLUE TEAM</h1>
          </div>
          <motion.div className='gallery'>
            <motion.div className='slider' drag='x' 
             dragConstraints={{right: 0, left:-3100}} >
              {blue.map((item, index) => (
                <motion.div key={index} className="item">
                  <img 
                  src={require(`../../assets/images/artist/01e/${item.image}`)} 
                  alt="Product" 
                  className="ai-img" />
                </motion.div>
              ))}
            </motion.div>
          </motion.div>
        </div>
        <div className='profileCont'>
            <div>
              <img src={Blue} alt='alexa_picture' className='imgArtist'/>
            </div>
            <div>
              <p className='profileText'>Blue Team es un grupo de Artistas visuales apasionados, 
                con un historial destacado en diferentes medios artísticos. 
                Gran experiencia en ilustración y animación. Acostumbrados a 
                trabajar de forma independiente y en colaboración en proyectos diversos, 
                siempre buscando la innovación y la belleza visual. </p>
                <p>
                  Contact: <span className='email'>blueTeam_ia_art@works.com</span>
                </p>
            </div>
        </div>    
      </div>
    </>
  )
}

export default ArtistBlueTeam