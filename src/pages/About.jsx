import React, { useEffect, useRef } from 'react';

const About = () => {
  const textareaRef = useRef(null);
  const voiceListRef = useRef(null);
  const speechBtnRef = useRef(null);

  useEffect(() => {
    const textarea = textareaRef.current;
    const voiceList = voiceListRef.current;
    const speechBtn = speechBtnRef.current;

    let synth = speechSynthesis;
    let isSpeaking = true;

    function voices(voiceList) {
      for (let voice of synth.getVoices()) {
        let selected = voice.name === "Google US English" ? "selected" : "";
        let option = `<option value="${voice.name}" ${selected}>${voice.name} (${voice.lang})</option>`;
        voiceList.insertAdjacentHTML("beforeend", option);
      }
    }

    synth.addEventListener("voiceschanged", () => {
      voices(voiceList);
    });

    function textToSpeech(text) {
      let utterance = new SpeechSynthesisUtterance(text);
      for (let voice of synth.getVoices()) {
        if (voice.name === voiceList.value) {
          utterance.voice = voice;
        }
      }
      synth.speak(utterance);
    }

    speechBtn.addEventListener("click", (e) => {
      e.preventDefault();
      if (textarea.value !== "") {
        if (!synth.speaking) {
          textToSpeech(textarea.value);
        }
        if (textarea.value.length > 280) {
          setInterval(() => {
            if (!synth.speaking && !isSpeaking) {
              isSpeaking = true;
              speechBtn.innerText = "Convert To Speech";
            } else {
            }
          }, 500);
          if (isSpeaking) {
            synth.resume();
            isSpeaking = false;
            speechBtn.innerText = "Pause Speech";
          } else {
            synth.pause();
            isSpeaking = true;
            speechBtn.innerText = "Resume Speech";
          }
        } else {
          speechBtn.innerText = "Convert To Speech";
        }
      }
    });
  }, []);

  return (
    <>
      <div className='AboutContainer'>
        <div className="wrapper">
          <header>Text to speech</header>
          <form action="#">
            <div className="row">
              <label>Enter Text</label>
              <textarea ref={textareaRef}>Our mission is to encourage 
              and help visual artists and enable them to create. 
              Even in difficult times. We do that by providing tools 
              and the platform to show and promote their artwork and 
              assisting along the way. </textarea>
            </div>
            <div className="row">
              <label>Select Voice</label>
              <div className="outer">
                <select ref={voiceListRef}></select>
              </div>
            </div>
            <button ref={speechBtnRef}>Convert To Speech</button>
          </form>
        </div>
        <div className='AboutUsText'>
          <h1 className='title-main'>About Us</h1>
            <p className='par-main'>Our mission is to encourage and 
            help visual artists and enable them to create. <br/>
            Even in difficult times. 
            We do that by providing tools and the platform to show 
            and promote their artwork and assisting along the way. 
            The foundation also maintains a space to advertise artists, 
            bring them closer to the public and allow people to support 
            their projects.<br/>
            </p>
          <h1 className='title-main'>Events</h1>
            <p className='par-main'>
            We can organize for you or participate as a partner in charity events. 
            If you are interested in cooperating or co-create an event, 
            please contact us at <span className='email'>ia_art@contact.art</span>.
            </p>
        </div>
      </div>
    </>
  );
};

export default About;
