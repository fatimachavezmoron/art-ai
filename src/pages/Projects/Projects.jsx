import React from 'react'
// import ReactPlayer from 'react-player'
// import Video from '../../Video/pexels-mart-production-7565889.mp4'
import { motion } from 'framer-motion';


const Projects = () => {
  return (
    <>
      <div className='projectsCont'>
        {/* <div className='videoCont'>
          <ReactPlayer 
            url={Video} 
            controls
            width={350}
            height={470}
          />
        </div> */}
        <div className='MuseumCont'>
        <motion.div
           initial='hidden'
           animate='visible'
           variants={{
             hidden: { opacity: 0, x: -50 },
             visible: { opacity: 1, x: 0 }
           }}
           transition={{ delay: 0.3, duration: 1.9 }}
           whileInView='visible'
           viewport={{ once: true, threshold: 0.5 }}
        >
          <h1 className='artistName3'> TOUR 2025 SHOWS ART MADE WITH ARTIFICIAL INTELLIGENCE.</h1>
          <h3 className='contactProjects'>Contact Us</h3>
          <div class="arrow-container">
            <div class="arrow"> ▼ </div>
          </div>
        </motion.div>
        <motion.div 
           initial='hidden'
           animate='visible'
           variants={{
             hidden: { opacity: 0, x: -50 },
             visible: { opacity: 1, x: 0 }
           }}
           transition={{ delay: 0.3, duration: 1.9 }}
           whileInView='visible'
           viewport={{ once: true, threshold: 0.5 }}
        >
          <h3 className='contactProjects2'>contactUs@artTour.com</h3>
        </motion.div>  
          <h2>Museums that provide a space for our art:</h2>
          <ul>
            <li>Rijksmuseum, Amsterdam</li>
            <li>Acropolis Museum, Athens</li>
            <li>National Gallery, London</li>
            <li>Georges Pompidou Center, Paris</li>
            <li>Serralves Museum, Porto</li>
            <li>Prado Museum, Madrid</li>
          </ul>
        </div>
      </div>
    </>
  )
}

export default Projects